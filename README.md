## Mini Dungeon Game

### Introduction
Mini-Dungeon is a single-player turn-based game. Player controls the hero to walk around in the map, with the goal of arriving the exit.
### Hero
Controlled by arrow keys
Speed : 1
Maximum HP : 40
### Monster
Non-playable
speed : 1 (when finding the hero)
Attack : 6-15
### Potion
Non-playable
Heal : 10
### Treasure
Non-playable
Reward : 10-110
### Reward
When Score reaches 200 or more, HP of the hero will be fulfilled if the hero is injured.     
